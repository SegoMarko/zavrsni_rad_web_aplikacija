package Zavrsni_rad.web_aplikacija.dao;

import Zavrsni_rad.web_aplikacija.domain.Predmet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PredmetRepository extends JpaRepository<Predmet,Integer> {
    Predmet findByIdPredmet(int idPredmet);

    List<Predmet> findByNazivPredmetContaining(String nazivPredmet);

    @Query("SELECT p FROM Predmet p JOIN DopustenjePredmet dp" +
            " ON p.idPredmet=dp.idPredmet" +
            " WHERE dp.idKorisnik=(:idKorisnik)" +
            " AND p.nazivPredmet LIKE '%(:nazivPredmet)%'")
    List<Predmet> findByNazivPredmet(@Param("nazivPredmet") String nazivPredmet,@Param("idKorisnik") int idKorisnik);
}
