package Zavrsni_rad.web_aplikacija.service.impl;

import Zavrsni_rad.web_aplikacija.dao.KvizRepository;
import Zavrsni_rad.web_aplikacija.domain.Kviz;
import Zavrsni_rad.web_aplikacija.domain.KvizComparator;
import Zavrsni_rad.web_aplikacija.service.KvizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class KvizJpa implements KvizService {
    @Autowired
    private KvizRepository kvizRepository;

    @Override
    public Kviz saveKviz(Kviz kviz) {
        return kvizRepository.saveAndFlush(kviz);
    }

    @Override
    public Kviz[] findByImeKviz(String imeKviz, int idKorisnik) {
        return kvizRepository.findByImeKvizCustom(imeKviz, idKorisnik);
    }

    @Override
    public List<Kviz> getKvizByIdNastCjelCustom(int idNastavnaCjelina, int idKorisnik) {
        List<Kviz> l = kvizRepository.findByIdNastCjelCustom(idNastavnaCjelina, idKorisnik);
        l.sort(new KvizComparator());
        return l;
    }

    @Override
    public List<Kviz> getKvizByIdNastCjel(int idNastavnaCjelina) {
        List<Kviz> l = kvizRepository.findByIdNastCjel(idNastavnaCjelina);
        l.sort(new KvizComparator());
        return l;
    }

    @Override
    public void deleteKviz(int idKviz) {
        this.kvizRepository.deleteById(idKviz);
    }

    @Override
    public Kviz getKvizById(int idKviz) {
        Optional<Kviz> k = kvizRepository.findById(idKviz);
        if(k.isPresent()) {
            return k.get();
        }else{
            return null;
        }
    }

    @Override
    public List<Kviz> listAll(){
        return kvizRepository.findAll();
    }
}
