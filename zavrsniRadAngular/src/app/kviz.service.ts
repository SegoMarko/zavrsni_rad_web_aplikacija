import { Injectable } from '@angular/core';
import { Kviz } from './kviz';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { NastavnaCjelina } from './nastavnaCjelina';
import { tap } from 'rxjs/operators'
import { PomPorukeService } from './pom-poruke.service';

@Injectable({
  providedIn: 'root'
})
export class KvizService {
  private KvizURL='http://localhost:8080/kviz';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  private idKorisnik=1;

  constructor(private http: HttpClient, private pomPorukeService: PomPorukeService) { }

  addKviz(kviz: Kviz): Observable<Kviz>{
    return this.http.post<Kviz>(this.KvizURL+"/"+this.idKorisnik, kviz, this.httpOptions)
      .pipe(
        tap( _ => this.pomPorukeService.add(`Kviz ${kviz.imeKviz} dodan`))
      );
  }

  getKviz(id: number): Observable<Kviz> {
    return this.http.get<Kviz>(this.KvizURL+"/"+id, this.httpOptions);
  }

  updateKviz(kviz: Kviz): Observable<any>{
    return this.http.put<Kviz>(this.KvizURL, kviz, this.httpOptions)
      .pipe(
        tap( _ => this.pomPorukeService.add(`Kviz ${kviz.imeKviz} ažuriran`))
      );
  }

  deleteKviz(kviz: Kviz): Observable<Kviz>{
    return this.http.delete<Kviz>(this.KvizURL+"/"+kviz.idKviz, this.httpOptions)
      .pipe(
        tap( _ => this.pomPorukeService.add(`Kviz ${kviz.imeKviz} izbrisan`))
      );
  }

  getKvizovi(idNastavnaCjelina: number): Observable<Kviz[]>{
    return this.http.get<Kviz[]>(this.KvizURL+"/cjelina/"+idNastavnaCjelina+"/"+this.idKorisnik,this.httpOptions)
  }
  
  searchKviz(term: string): Observable<Kviz[]>{
    if (!term.trim()) {
      return of([]);
    }
    return this.http.get<Kviz[]>(this.KvizURL+"/naziv/"+term+"/"+this.idKorisnik);
  }
}
