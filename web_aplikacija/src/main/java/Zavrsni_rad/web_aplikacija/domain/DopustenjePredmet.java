package Zavrsni_rad.web_aplikacija.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="dopustenje_predmet")
@IdClass(DopustenjePredmetKey.class)
public class DopustenjePredmet {
    @Id
    private int idPredmet;
    @Id
    private int idKorisnik;
    private char dopustenje;

    public DopustenjePredmet() {}

    public DopustenjePredmet(int idPredmet, int idKorisnik, char dopustenje) {
        this.idPredmet = idPredmet;
        this.idKorisnik = idKorisnik;
        this.dopustenje = dopustenje;
    }

    public int getIdPredmet() {
        return idPredmet;
    }

    public void setIdPredmet(int idPredmet) {
        this.idPredmet = idPredmet;
    }

    public int getIdKorisnik() {
        return idKorisnik;
    }

    public void setIdKorisnik(int idKorisnik) {
        this.idKorisnik = idKorisnik;
    }

    public char getDopustenje() {
        return dopustenje;
    }

    public void setDopustenje(char dopustenje) {
        this.dopustenje = dopustenje;
    }
}
