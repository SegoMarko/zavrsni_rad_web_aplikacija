export class Kviz{
    idKviz: number;
    idNastCjel: number;
    imeKviz: string;
    kratkiOpisKviz: string;
    opisKviz: string;
    redniBrojKviz: number;
    idKvizPovijest: number;
}