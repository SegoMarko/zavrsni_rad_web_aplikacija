export class PonudeniOdgovor{
    idPonudeniOdgovor: number;
    idPitanje: number;
    tekstOdg: string;
    tocanOdg: boolean;
}