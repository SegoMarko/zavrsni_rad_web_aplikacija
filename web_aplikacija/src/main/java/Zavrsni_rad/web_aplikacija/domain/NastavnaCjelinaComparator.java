package Zavrsni_rad.web_aplikacija.domain;

import java.util.Comparator;

public class NastavnaCjelinaComparator implements Comparator<NastavnaCjelina> {
    @Override
    public int compare(NastavnaCjelina o1, NastavnaCjelina o2) {
        if (o1.getRedniBrojNastCjel()>o2.getRedniBrojNastCjel())
            return 1;
        else if(o1.getRedniBrojNastCjel()==o2.getRedniBrojNastCjel())
            return 0;
        else
            return -1;
    }
}
