package Zavrsni_rad.web_aplikacija.service;

import Zavrsni_rad.web_aplikacija.domain.Predmet;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface PredmetService {

    List<Predmet> listAll();
    Predmet getPredmet(int idPredmet);
    Predmet savePredmet(Predmet predmet);
    void deletePredmet(int idPredmet);

    List<Predmet> findByNazivPredmet(String nazivPredmet, int idKorisnik);

    List<Predmet> listPredmet(int idKorisnik);
}
