package Zavrsni_rad.web_aplikacija.service;

import Zavrsni_rad.web_aplikacija.domain.PitanjePovijest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PitanjePovijestService {
    List<PitanjePovijest> getPitanjePovijestByIdKvizPovijest(int idKvizPovijest);
}
