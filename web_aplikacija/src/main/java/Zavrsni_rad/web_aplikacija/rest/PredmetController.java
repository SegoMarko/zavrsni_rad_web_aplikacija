package Zavrsni_rad.web_aplikacija.rest;

import Zavrsni_rad.web_aplikacija.domain.DopustenjePredmet;
import Zavrsni_rad.web_aplikacija.domain.Predmet;
import Zavrsni_rad.web_aplikacija.domain.RazinaStudij;
import Zavrsni_rad.web_aplikacija.service.DopustenjePredmetService;
import Zavrsni_rad.web_aplikacija.service.PredmetService;
import Zavrsni_rad.web_aplikacija.service.RazinaStudijService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/predmet")
public class PredmetController {
    @Autowired
    private PredmetService predmetService;
    @Autowired
    private DopustenjePredmetService dopustenjePredmetService;
    @Autowired
    private RazinaStudijService razinaStudijService;

    @GetMapping("/dopusteni/{idKorisnik}")
    public List<Predmet> listPredmet(@PathVariable int idKorisnik){
        return predmetService.listPredmet(idKorisnik);
    }

    @GetMapping("/{idPredmet}")
    public Predmet getPredmet(@PathVariable int idPredmet){
        return predmetService.getPredmet(idPredmet);
    }

    @PutMapping("")
    public Predmet updatePredmet(@RequestBody Predmet predmet){
        return predmetService.savePredmet(predmet);
    }

    @PostMapping("/{idKorisnik}")
    public Predmet createPredmet(@PathVariable int idKorisnik,@RequestBody Predmet predmet){
        Predmet p = predmetService.savePredmet(new Predmet(
                predmet.getNazivPredmet(),predmet.getIdRazStud(),
                predmet.getNastProg()));
        dopustenjePredmetService.saveDopustenje(new DopustenjePredmet(p.getIdPredmet(),idKorisnik,'o'));
        p.setRazinaStudij(razinaStudijService.getRazinaStudijById(p.getIdRazStud()));

        return p;
    }

    @DeleteMapping("/{id}")
    public void deletePredmet(@PathVariable int id){
        predmetService.deletePredmet(id);
    }

    @GetMapping("/naziv/{nazivPredmet}/{idKorisnik}")
    public List<Predmet> findByNazivPredmet(@PathVariable String nazivPredmet, @PathVariable int idKorisnik){
        return predmetService.findByNazivPredmet(nazivPredmet, idKorisnik);
    }

    @GetMapping("/razineStudij")
    public List<RazinaStudij> getRazineStuidj(){
        return razinaStudijService.getRazineStudij();
    }
}
