import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
 
import {
   debounceTime, distinctUntilChanged, switchMap
 } from 'rxjs/operators';
 
import { Predmet } from '../predmet';
import { PredmetService } from '../predmet.service';

@Component({
  selector: 'app-predmet-search',
  templateUrl: './predmet-search.component.html',
  styleUrls: ['./predmet-search.component.css']
})
export class PredmetSearchComponent implements OnInit {
  predmeti$: Observable<Predmet[]>;
  predmeti: Predmet[];
  private searchTerms = new Subject<string>();
 
  constructor(private predmetService: PredmetService) {}
 
  // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
  }
 
  ngOnInit(): void {
    this.predmetService.getPredmeti().subscribe(predmeti => this.predmeti=predmeti);
    this.predmeti$ = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),
 
      // ignore new term if same as previous term
      distinctUntilChanged(),
 
      // switch to new search observable each time the term changes
      switchMap((term: string) => this.predmetService.filterPredmeti(term, this.predmeti)),
    );
  }
}