package Zavrsni_rad.web_aplikacija.dao;

import Zavrsni_rad.web_aplikacija.domain.Kviz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KvizRepository extends JpaRepository<Kviz, Integer> {
    //@Query("SELECT k FROM Kviz k" +
    //        " WHERE k.idNastCjel=(:idNastavnaCjelina)")
    List<Kviz> findByIdNastCjel(int idNastCjel);

    @Query("SELECT k FROM Kviz k JOIN DopustenjeKviz dk" +
            " ON k.idKviz=dk.idKviz" +
            " WHERE dk.idKorisnik=(:idKorisnik)" +
            " AND k.imeKviz LIKE CONCAT('%',:imeKviz,'%')")
    Kviz[] findByImeKvizCustom(@Param("imeKviz") String imeKviz, @Param("idKorisnik") int idKorisnik);

    @Query("SELECT k FROM Kviz k JOIN DopustenjeKviz dk" +
            " ON k.idKviz=dk.idKviz" +
            " WHERE k.idNastCjel=(:idNastavnaCjelina)" +
            " AND dk.idKorisnik=(:idKorisnik)")
    List<Kviz> findByIdNastCjelCustom(@Param("idNastavnaCjelina") int idNastavnaCjelina, @Param("idKorisnik") int idKorisnik);
}
