package Zavrsni_rad.web_aplikacija.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name="odgovor_student")
@IdClass(OdgovorStudentKey.class)
public class OdgovorStudent {
    @Id
    private int idStudent;
    @Id
    private int idTest;
    @Id
    private int idOdgovor;
    private LocalDateTime vrijemeOdg;

    public OdgovorStudent() {}

    public OdgovorStudent(int idStudent, int idTest, int idOdgovor, LocalDateTime vrijemeOdg) {
        this.idStudent = idStudent;
        this.idTest = idTest;
        this.idOdgovor = idOdgovor;
        this.vrijemeOdg = vrijemeOdg;
    }

    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public int getIdTest() {
        return idTest;
    }

    public void setIdTest(int idTest) {
        this.idTest = idTest;
    }

    public int getIdOdgovor() {
        return idOdgovor;
    }

    public void setIdOdgovor(int idOdgovor) {
        this.idOdgovor = idOdgovor;
    }

    public LocalDateTime getVrijemeOdg() {
        return vrijemeOdg;
    }

    public void setVrijemeOdg(LocalDateTime vrijemeOdg) {
        this.vrijemeOdg = vrijemeOdg;
    }
}
