package Zavrsni_rad.web_aplikacija.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Entity
@Table(name="predmet")
public class Predmet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPredmet;
    private String nazivPredmet;
    private int idRazStud;
    private String nastProg;

    @ManyToOne
    @JoinColumn(name = "idRazStud", insertable = false, updatable = false,
            referencedColumnName = "idRazinaStudij", nullable = false)
    //@JsonIgnore
    @Fetch(FetchMode.JOIN)
    private RazinaStudij razinaStudij;

    public Predmet(){}

    public Predmet(String nazivPredmet, int idRazStud, String nastProg) {
        this.nazivPredmet = nazivPredmet;
        this.idRazStud = idRazStud;
        this.nastProg = nastProg;
    }

    public Predmet(int idPredmet, String nazivPredmet, int idRazStud, String nastProg) {
        this.idPredmet = idPredmet;
        this.nazivPredmet = nazivPredmet;
        this.idRazStud = idRazStud;
        this.nastProg = nastProg;
    }

    public int getIdPredmet() {
        return idPredmet;
    }

    public void setIdPredmet(int idPredmet) {
        this.idPredmet = idPredmet;
    }

    public String getNazivPredmet() {
        return nazivPredmet;
    }

    public void setNazivPredmet(String nazivPredmet) {
        this.nazivPredmet = nazivPredmet;
    }

    public int getIdRazStud() {
        return idRazStud;
    }

    public void setIdRazStud(int idRazStud) {
        this.idRazStud = idRazStud;
    }

    public String getNastProg() {
        return nastProg;
    }

    public void setNastavniProgram(String nastProg) {
        this.nastProg = nastProg;
    }

    public RazinaStudij getRazinaStudij() {
        return razinaStudij;
    }

    public void setRazinaStudij(RazinaStudij razinaStudij) {
        this.razinaStudij = razinaStudij;
    }
}
