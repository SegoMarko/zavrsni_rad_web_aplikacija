package Zavrsni_rad.web_aplikacija.dao;

import Zavrsni_rad.web_aplikacija.domain.DopustenjeCjelina;
import Zavrsni_rad.web_aplikacija.domain.DopustenjeCjelinaKey;
import Zavrsni_rad.web_aplikacija.domain.NastavnaCjelina;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DopustenjeCjelinaRepository extends JpaRepository<DopustenjeCjelina, DopustenjeCjelinaKey> {

    /*@Query("SELECT DISTINCT nc FROM NastavnaCjelina nc" +
            " LEFT JOIN DopustenjeCjelina dc ON" +
            " nc.idNastavnaCjelina = dc.idNastavnaCjelina" +
            " LEFT JOIN Predmet p ON" +
            " p.idPredmet = nc.idPredmet " +
            " WHERE dc.idKorisnik=(:idKorisnik)")
    List<NastavnaCjelina> dopustenByIdPredmet(@Param("idPredmet")int idPredmet,@Param("idKorisnik") int idKorisnik);
     */
}
