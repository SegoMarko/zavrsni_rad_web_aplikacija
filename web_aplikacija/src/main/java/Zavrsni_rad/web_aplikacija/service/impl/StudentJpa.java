package Zavrsni_rad.web_aplikacija.service.impl;

import Zavrsni_rad.web_aplikacija.dao.StudentRepository;
import Zavrsni_rad.web_aplikacija.domain.Student;
import Zavrsni_rad.web_aplikacija.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentJpa implements StudentService {
    @Autowired
    private StudentRepository studentRepository;

    @Override
    public List<Student> listAll() {
        return studentRepository.findAll();
    }
}
