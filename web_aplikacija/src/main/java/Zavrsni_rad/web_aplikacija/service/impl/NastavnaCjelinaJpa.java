package Zavrsni_rad.web_aplikacija.service.impl;

import Zavrsni_rad.web_aplikacija.dao.DopustenjeCjelinaRepository;
import Zavrsni_rad.web_aplikacija.dao.NastavnaCjelinaRepository;
import Zavrsni_rad.web_aplikacija.domain.DopustenjeCjelina;
import Zavrsni_rad.web_aplikacija.domain.NastavnaCjelina;
import Zavrsni_rad.web_aplikacija.domain.NastavnaCjelinaComparator;
import Zavrsni_rad.web_aplikacija.service.NastavnaCjelinaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class NastavnaCjelinaJpa implements NastavnaCjelinaService {
    @Autowired
    private NastavnaCjelinaRepository nastavnaCjelinaRepository;
    @Autowired
    private DopustenjeCjelinaRepository dopustenjeCjelinaRepository;

    @Override
    public void deleteNastavnaCjelina(int idNastavnaCjelina) {
        nastavnaCjelinaRepository.deleteById(idNastavnaCjelina);
    }

    @Override
    public List<NastavnaCjelina> findByNazNastCjel(String nazNastCjel, int idKorisnik) {
        List<NastavnaCjelina> list = nastavnaCjelinaRepository.findByNazNastCjelCustom(nazNastCjel, idKorisnik);
        list.sort(new NastavnaCjelinaComparator());
        return list;
    }

    @Override
    public NastavnaCjelina saveNastavnaCjelina(NastavnaCjelina nastavnaCjelina) {
        return nastavnaCjelinaRepository.saveAndFlush(nastavnaCjelina);
    }

    /*@Override
    public NastavnaCjelina saveNastavnaCjelinaIDopustenje(int idKorisnik, NastavnaCjelina nastavnaCjelina) {
        NastavnaCjelina n = nastavnaCjelinaRepository.saveAndFlush(nastavnaCjelina);
        System.out.println("idNastavnaCjelina = " + n.getIdNastavnaCjelina());
        dopustenjeCjelinaRepository.save(new DopustenjeCjelina(idKorisnik, n.getIdNastavnaCjelina(), 'o'));

        return n;
    }*/

    @Override
    public NastavnaCjelina findByIdNastavnaCjelina(int idNastavnaCjelina) {
        Optional<NastavnaCjelina> rez = nastavnaCjelinaRepository.findById(idNastavnaCjelina);
        if (rez.isPresent())
            return rez.get();
        return null;
    }

    @Override
    public List<NastavnaCjelina> findByIdPredmet(int idPredmet) {
        List<NastavnaCjelina> list = nastavnaCjelinaRepository.findByIdPredmet(idPredmet);
        list.sort(new NastavnaCjelinaComparator());
        return list;
    }

    @Override
    public List<NastavnaCjelina> findByIdPredmetCustom(int idPredmet, int idKorisnik) {
        List<NastavnaCjelina> list = nastavnaCjelinaRepository.findByIdPredmetCustom(idPredmet, idKorisnik);
        list.sort(new NastavnaCjelinaComparator());
        return list;
    }

    @Override
    public List<NastavnaCjelina> listAll(){
        return nastavnaCjelinaRepository.findAll();
    }


}
