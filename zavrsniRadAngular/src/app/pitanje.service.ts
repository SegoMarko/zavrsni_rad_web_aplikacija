import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pitanje } from './pitanje';
import { PonudeniOdgovor } from './ponudeniOdgovor';
import { PomPorukeService } from './pom-poruke.service';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PitanjeService {
  private pitanjeUrl='http://localhost:8080/pitanje';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  private idKorisnik=1;

  constructor(private http: HttpClient, private pomPorukeService: PomPorukeService) { }

  getPitanja(idKviz: number): Observable<Pitanje[]>{
    return this.http.get<Pitanje[]>(this.pitanjeUrl+"/"+idKviz+"/"+this.idKorisnik,this.httpOptions);
  }

  getPitanje(idPitanje: number): Observable<Pitanje>{
    return this.http.get<Pitanje>(this.pitanjeUrl+"/pitanje/"+idPitanje, this.httpOptions);
  }

  addPitanje(pitanje: Pitanje, idKviz: number): Observable<Pitanje>{
    return this.http.post<Pitanje>(this.pitanjeUrl+"/"+this.idKorisnik+"/"+idKviz, pitanje, this.httpOptions)
      .pipe(
        tap( _ => this.pomPorukeService.add(`Dodano novo pitanje`))
      );
  }

  savePitanje(pitanje: Pitanje): Observable<Pitanje>{
    return this.http.put<Pitanje>(this.pitanjeUrl+"/"+this.idKorisnik, pitanje, this.httpOptions)
      .pipe(
        tap( _ => this.pomPorukeService.add(`Pitanje ažurirano`))
      );
  }

  addOdgovor(odgovor: PonudeniOdgovor): Observable<PonudeniOdgovor>{
    return this.http.post<PonudeniOdgovor>(this.pitanjeUrl, odgovor, this.httpOptions)
      .pipe(
        tap( _ => this.pomPorukeService.add(`Dodan novi odgovor`))
      );
  }

  deletePitanje(pitanje: Pitanje, idKviz: number): Observable<any>{
    return this.http.delete<Pitanje>(this.pitanjeUrl+"/"+idKviz+"/"+pitanje.idPitanje, this.httpOptions)
      .pipe(
        tap( _ => this.pomPorukeService.add(`Pitanje izbrisano`))
      );
  }

  deleteOdgovor(odgovor: PonudeniOdgovor): Observable<any>{
    return this.http.delete<PonudeniOdgovor>(this.pitanjeUrl+"/"+odgovor.idPonudeniOdgovor, this.httpOptions)
      .pipe(
        tap( _ => this.pomPorukeService.add(`Odgovor izbrisan`))
      );
  }
}
