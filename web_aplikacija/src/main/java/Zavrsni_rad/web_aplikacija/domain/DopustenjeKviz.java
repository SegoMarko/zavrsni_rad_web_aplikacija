package Zavrsni_rad.web_aplikacija.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="dopustenje_kviz")
@IdClass(DopustenjeKvizKey.class)
public class DopustenjeKviz {
    @Id
    private int idKorisnik;
    @Id
    private int idKviz;
    private char dopustenje;

    public DopustenjeKviz() {}

    public DopustenjeKviz(int idKorisnik, int idKviz, char dopustenje) {
        this.idKorisnik = idKorisnik;
        this.idKviz = idKviz;
        this.dopustenje = dopustenje;
    }

    public int getIdKorisink() {
        return idKorisnik;
    }

    public void setIdKorisink(int idKorisink) {
        this.idKorisnik = idKorisink;
    }

    public int getIdKviz() {
        return idKviz;
    }

    public void setIdKviz(int idKviz) {
        this.idKviz = idKviz;
    }

    public char getDopustenje() {
        return dopustenje;
    }

    public void setDopustenje(char dopustenje) {
        this.dopustenje = dopustenje;
    }
}
