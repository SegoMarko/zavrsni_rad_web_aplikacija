package Zavrsni_rad.web_aplikacija.dao;

import Zavrsni_rad.web_aplikacija.domain.PitanjePovijest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PitanjePovijestRepository extends JpaRepository<PitanjePovijest,Integer> {
    List<PitanjePovijest> findByIdKvizPovijest(int idKvizPovijest);
}
