package Zavrsni_rad.web_aplikacija.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="kviz_pitanje")
@IdClass(KvizPitanjeKey.class)
public class KvizPitanje {
    @Id
    private int idKviz;
    @Id
    private int idPitanje;
    private int redniBrojPitanje;

    public KvizPitanje() {}

    public KvizPitanje(int idKviz, int idPitanje, int redniBrojPitanje) {
        this.idKviz = idKviz;
        this.idPitanje = idPitanje;
        this.redniBrojPitanje = redniBrojPitanje;
    }

    public int getIdKviz() {
        return idKviz;
    }

    public void setIdKviz(int idKviz) {
        this.idKviz = idKviz;
    }

    public int getIdPitanje() {
        return idPitanje;
    }

    public void setIdPitanje(int idPitanje) {
        this.idPitanje = idPitanje;
    }

    public int getRedniBrojPitanje() {
        return redniBrojPitanje;
    }

    public void setRedniBrojPitanje(int redniBrojPitanje) {
        this.redniBrojPitanje = redniBrojPitanje;
    }
}
