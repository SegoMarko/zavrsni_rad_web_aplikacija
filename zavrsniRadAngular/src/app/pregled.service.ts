import { Injectable } from '@angular/core';
import { Predmet } from './predmet';
import { NastavnaCjelina } from './nastavnaCjelina';

@Injectable({
  providedIn: 'root'
})
export class PregledService {
  private trenutniPredmet: Predmet={
    idPredmet:0,
    nazivPredmet:'',
    idRazStud:0,
    nastProg:''
  };
  private trenutnaCjelina: NastavnaCjelina={
    idNastavnaCjelina:0,
    nazNastCjel: '',
    kratkiOpisNastCjel: '',
    opisNastCjel: '',
    idPredmet: 0,
    redniBrojNastCjel: 0
  }

  constructor() { }

  getTrenutniPredmet(): Predmet{
    return this.trenutniPredmet;
  }

  getTrenutnaCjelina(): NastavnaCjelina{
    return this.trenutnaCjelina;
  }

  setTrenutniPredmet(predmet: Predmet): void{
    this.trenutniPredmet=predmet;
  }

  setTrenutnaCjelina(cjelina: NastavnaCjelina): void{
    this.trenutnaCjelina=cjelina;
  }
}
