package Zavrsni_rad.web_aplikacija.service;

import Zavrsni_rad.web_aplikacija.domain.KvizPovijest;
import Zavrsni_rad.web_aplikacija.domain.ObavljenTest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ObavljenTestService {
    List<ObavljenTest> listAll();

    List<ObavljenTest> getKvizPovijestByIdKorisnik(int idKorisnik);
}
