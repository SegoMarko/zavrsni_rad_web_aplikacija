package Zavrsni_rad.web_aplikacija.service.impl;

import Zavrsni_rad.web_aplikacija.dao.ObavljenTestRepository;
import Zavrsni_rad.web_aplikacija.domain.KvizPovijest;
import Zavrsni_rad.web_aplikacija.domain.ObavljenTest;
import Zavrsni_rad.web_aplikacija.service.ObavljenTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ObavljenTestJpa implements ObavljenTestService {
    @Autowired
    private ObavljenTestRepository obavljenTestRepository;

    @Override
    public List<ObavljenTest> listAll() {
        return obavljenTestRepository.findAll();
    }

    @Override
    public List<ObavljenTest> getKvizPovijestByIdKorisnik(int idKorisnik) {
        return obavljenTestRepository.findByIdKorisnik(idKorisnik);
    }
}
