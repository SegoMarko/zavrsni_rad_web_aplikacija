package Zavrsni_rad.web_aplikacija.domain;

import javax.persistence.*;

@Entity
@Table(name="kviz_povijest")
public class KvizPovijest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idKvizPovijest;
    //private int idObavljenTest;
    private String imePredmet;
    private String imeNastCjel;
    private String imeKviz;
    private String opisKviz;

    /*@OneToMany
    @JoinColumn(name = "idObavljenTest", insertable = false, updatable = false,
            referencedColumnName = "idObavljenTest", nullable = false)
    private ObavljenTest obavljenTest;*/

    public KvizPovijest() {}

    public KvizPovijest(String imePredmet, String imeNastCjel, String imeKviz, String opisKviz) {
        this.imePredmet = imePredmet;
        this.imeNastCjel = imeNastCjel;
        this.imeKviz = imeKviz;
        this.opisKviz = opisKviz;
    }

    public KvizPovijest(int idKvizPovijest, String imePredmet, String imeNastCjel, String imeKviz, String opisKviz) {
        this.idKvizPovijest = idKvizPovijest;
        this.imePredmet = imePredmet;
        this.imeNastCjel = imeNastCjel;
        this.imeKviz = imeKviz;
        this.opisKviz = opisKviz;
    }

    public int getIdKvizPovijest() {
        return idKvizPovijest;
    }

    public void setIdKvizPovijest(int idKvizPovijest) {
        this.idKvizPovijest = idKvizPovijest;
    }

    public String getImePredmet() {
        return imePredmet;
    }

    public void setImePredmet(String imePredmet) {
        this.imePredmet = imePredmet;
    }

    public String getImeNastCjel() {
        return imeNastCjel;
    }

    public void setImeNastCjel(String imeNastCjel) {
        this.imeNastCjel = imeNastCjel;
    }

    public String getImeKviz() {
        return imeKviz;
    }

    public void setImeKviz(String imeKviz) {
        this.imeKviz = imeKviz;
    }

    public String getOpisKviz() {
        return opisKviz;
    }

    public void setOpisKviz(String opisKviz) {
        this.opisKviz = opisKviz;
    }

}
