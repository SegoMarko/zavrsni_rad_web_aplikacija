package Zavrsni_rad.web_aplikacija.domain;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Entity
@Table(name="ponudeni_odgovor")
public class PonudeniOdgovor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPonudeniOdgovor;
    private int idPitanje;
    private String tekstOdg;
    private boolean tocanOdg;

    @ManyToOne
    @JoinColumn(name = "idPitanje", insertable = false, updatable = false,
            referencedColumnName = "idPitanje", nullable = false)
    @Fetch(FetchMode.JOIN)
    private Pitanje pitanje;

    public PonudeniOdgovor() {}

    public PonudeniOdgovor(int idPonudeniOdgovor, int idPitanje, String tekstOdg, boolean tocanOdg) {
        this.idPonudeniOdgovor = idPonudeniOdgovor;
        this.idPitanje = idPitanje;
        this.tekstOdg = tekstOdg;
        this.tocanOdg = tocanOdg;
    }

    public int getIdPonudeniOdgovor() {
        return idPonudeniOdgovor;
    }

    public void setIdPonudeniOdgovor(int idPonudeniOdgovor) {
        this.idPonudeniOdgovor = idPonudeniOdgovor;
    }

    public int getIdPitanje() {
        return idPitanje;
    }

    public void setIdPitanje(int idPitanje) {
        this.idPitanje = idPitanje;
    }

    public String getTekstOdg() {
        return tekstOdg;
    }

    public void setTekstOdg(String tekstOdg) {
        this.tekstOdg = tekstOdg;
    }

    public boolean isTocanOdg() {
        return tocanOdg;
    }

    public void setTocanOdg(boolean tocanOdg) {
        this.tocanOdg = tocanOdg;
    }
}
