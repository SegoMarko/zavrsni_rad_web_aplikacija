package Zavrsni_rad.web_aplikacija.service;

import org.springframework.stereotype.Service;

@Service
public interface RezultatStudentService {
    int getBrStud(int idObavljenTest);

    int getTocniBodovi(int idObavljenTest);

    int getNetocniBodovi(int idObavljenTest);

    int getNeodgovoreniBodovi(int idObavljenTest);

    int getBrStudTocnoOdg(int idObavljenTest, int idPitanjePovijest);
}
