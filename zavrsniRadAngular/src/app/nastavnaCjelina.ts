export class NastavnaCjelina{
    idNastavnaCjelina: number;
    nazNastCjel: string;
    kratkiOpisNastCjel: string;
    opisNastCjel: string;
    idPredmet: number;
    redniBrojNastCjel: number;
}