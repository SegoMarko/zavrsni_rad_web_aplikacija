package Zavrsni_rad.web_aplikacija.service.impl;

import Zavrsni_rad.web_aplikacija.dao.DopustenjePitanjeRepository;
import Zavrsni_rad.web_aplikacija.domain.DopustenjePitanje;
import Zavrsni_rad.web_aplikacija.service.DopustenjePitanjeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DopustenjePitanjeJpa implements DopustenjePitanjeService {
    @Autowired
    private DopustenjePitanjeRepository dopustenjePitanjeRepository;

    @Override
    public DopustenjePitanje saveDopustenje(DopustenjePitanje dopustenjePitanje) {
        return dopustenjePitanjeRepository.saveAndFlush(dopustenjePitanje);
    }
}
