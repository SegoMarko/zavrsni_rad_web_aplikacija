import { RazinaStudij } from './razinaStudij';

export class Predmet{
    idPredmet: number;
    nazivPredmet: string;
    idRazStud: number;
    nastProg: string;
    //razinaStudij: RazinaStudij;
}