package Zavrsni_rad.web_aplikacija.service.impl;

import Zavrsni_rad.web_aplikacija.dao.RazinaStudijRepository;
import Zavrsni_rad.web_aplikacija.domain.RazinaStudij;
import Zavrsni_rad.web_aplikacija.service.RazinaStudijService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RazinaStudijJpa implements RazinaStudijService {
    @Autowired
    private RazinaStudijRepository razinaStudijRepository;

    @Override
    public RazinaStudij getRazinaStudijById(int idRazinaStudij) {
        Optional<RazinaStudij> razina = razinaStudijRepository.findById(idRazinaStudij);
        if(razina.isPresent())
            return razina.get();
        else
            return null;
    }

    @Override
    public List<RazinaStudij> getRazineStudij() {
        return razinaStudijRepository.findAll();
    }
}
