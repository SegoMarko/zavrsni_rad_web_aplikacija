package Zavrsni_rad.web_aplikacija.service;

import Zavrsni_rad.web_aplikacija.domain.DopustenjePredmet;
import Zavrsni_rad.web_aplikacija.domain.Predmet;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DopustenjePredmetService {
    List<DopustenjePredmet> listAll();
    List<Predmet> predmetZaKoristenje(Integer idKorisnik);

    DopustenjePredmet saveDopustenje(DopustenjePredmet dopustenjePredmet);
}
