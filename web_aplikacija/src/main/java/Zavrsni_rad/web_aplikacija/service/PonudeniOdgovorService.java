package Zavrsni_rad.web_aplikacija.service;

import Zavrsni_rad.web_aplikacija.domain.PonudeniOdgovor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PonudeniOdgovorService {
    List<PonudeniOdgovor> listAll();

    PonudeniOdgovor addPonudeniOdgovor(PonudeniOdgovor ponudeniOdgovor);

    void deletePonudeniOdgovor(int idOdgovor);
}
