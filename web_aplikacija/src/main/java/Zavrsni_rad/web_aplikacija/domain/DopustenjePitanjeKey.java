package Zavrsni_rad.web_aplikacija.domain;

import java.io.Serializable;

public class DopustenjePitanjeKey implements Serializable {
    private int idKorisnik;
    private int idPitanje;
}
