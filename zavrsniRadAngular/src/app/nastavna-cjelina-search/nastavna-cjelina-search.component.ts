import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { NastavnaCjelina } from '../nastavnaCjelina';
import { NastavnaCjelinaService } from '../nastavna-cjelina.service';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-nastavna-cjelina-search',
  templateUrl: './nastavna-cjelina-search.component.html',
  styleUrls: ['./nastavna-cjelina-search.component.css']
})
export class NastavnaCjelinaSearchComponent implements OnInit {
  nastavneCjeline$: Observable<NastavnaCjelina[]>;
  nastavneCjeline: NastavnaCjelina[];
  private searchTerms = new Subject<string>();

  constructor(private nastavnaCjelinaService: NastavnaCjelinaService) { }

  // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    //this.nastavnaCjelinaService.getNastavneCjeline().subscribe(nastavneCjeline => this.nastavneCjeline=nastavneCjeline);
    this.nastavneCjeline$=this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),
 
      // ignore new term if same as previous term
      distinctUntilChanged(),
      
      // switch to new search observable each time the term changes
      switchMap((term: string) => this.nastavnaCjelinaService.searchNastavneCjeline(term)),
    );
  }
}
