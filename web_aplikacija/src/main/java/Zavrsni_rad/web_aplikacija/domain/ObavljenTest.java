package Zavrsni_rad.web_aplikacija.domain;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="obavljen_test")
public class ObavljenTest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idObavljenTest;
    private int idKvizPovijest;
    private int idKorisnik;
    private LocalDateTime vrijemeTest;

    @ManyToOne
    @JoinColumn(name = "idKvizPovijest", insertable = false, updatable = false,
            referencedColumnName = "idKvizPovijest", nullable = false)
    @Fetch(FetchMode.JOIN)
    private KvizPovijest kvizPovijest;

    public ObavljenTest() {}

    public ObavljenTest(int idObavljenTest, int idKvizPovijest, int idKorisnik, LocalDateTime vrijemeTest) {
        this.idObavljenTest = idObavljenTest;
        this.idKvizPovijest = idKvizPovijest;
        this.idKorisnik = idKorisnik;
        this.vrijemeTest = vrijemeTest;
    }

    public int getIdObavljenTest() {
        return idObavljenTest;
    }

    public void setIdObavljenTest(int idObavljenTest) {
        this.idObavljenTest = idObavljenTest;
    }

    public int getIdKorisnik() {
        return idKorisnik;
    }

    public void setIdKorisnik(int idKorisnik) {
        this.idKorisnik = idKorisnik;
    }

    public LocalDateTime getVrijemeTest() {
        return vrijemeTest;
    }

    public void setVrijemeTest(LocalDateTime vrijemeTest) {
        this.vrijemeTest = vrijemeTest;
    }

    public int getIdKvizPovijest() {
        return idKvizPovijest;
    }

    public void setIdKvizPovijest(int idKvizPovijest) {
        this.idKvizPovijest = idKvizPovijest;
    }

    public KvizPovijest getKvizPovijest() {
        return kvizPovijest;
    }

    public void setKvizPovijest(KvizPovijest kvizPovijest) {
        this.kvizPovijest = kvizPovijest;
    }
}
