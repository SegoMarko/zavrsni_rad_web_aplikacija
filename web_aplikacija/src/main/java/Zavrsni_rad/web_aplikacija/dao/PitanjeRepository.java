package Zavrsni_rad.web_aplikacija.dao;

import Zavrsni_rad.web_aplikacija.domain.Pitanje;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PitanjeRepository extends JpaRepository<Pitanje, Integer> {

    @Query("SELECT p FROM Pitanje p JOIN KvizPitanje kp" +
            " ON p.idPitanje=kp.idPitanje" +
            " JOIN DopustenjePitanje dp" +
            " ON p.idPitanje=dp.idPitanje" +
            " WHERE dp.idKorisnik=(:idKorisnik)" +
            " AND (:idKviz)=kp.idKviz" +
            " ORDER BY kp.redniBrojPitanje")
    List<Pitanje> findByIdKvizCustom(@Param("idKviz") int idKviz, @Param("idKorisnik") int idKorisnik);
}
