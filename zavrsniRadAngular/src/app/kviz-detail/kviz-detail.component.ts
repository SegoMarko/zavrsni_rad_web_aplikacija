import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import { Kviz } from '../kviz'
import { ActivatedRoute } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { KvizService } from '../kviz.service';
import { Location } from '@angular/common'

@Component({
  selector: 'app-kviz-detail',
  templateUrl: './kviz-detail.component.html',
  styleUrls: ['./kviz-detail.component.css']
})
export class KvizDetailComponent implements OnInit {
  kviz: Kviz;
  modalRef: BsModalRef;

  constructor(private route: ActivatedRoute,private location: Location,
    private modalService: BsModalService, private kvizService: KvizService) { }

  ngOnInit() {
    this.getKviz();
  }

  goBack(): void{
    this.location.back();
  }

  save(): void {
    this.kvizService.updateKviz(this.kviz).subscribe(()=> this.goBack());
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  closeModal(){
    this.modalRef.hide();
  }

  getKviz(){
    const id = +this.route.snapshot.paramMap.get('id');
    this.kvizService.getKviz(id).subscribe(kviz=>this.kviz=kviz);
  }

  delete(): void{
    this.kvizService.deleteKviz(this.kviz).subscribe( kviz => { 
      this.modalRef.hide();
      this.location.back();
     });
  }
}
