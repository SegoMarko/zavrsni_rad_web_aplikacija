package Zavrsni_rad.web_aplikacija.dao;

import Zavrsni_rad.web_aplikacija.domain.OdgovorStudent;
import Zavrsni_rad.web_aplikacija.domain.OdgovorStudentKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OdgovorStudentRepository extends JpaRepository<OdgovorStudent, OdgovorStudentKey> {
}
