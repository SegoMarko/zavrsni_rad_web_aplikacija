package Zavrsni_rad.web_aplikacija.rest;

import Zavrsni_rad.web_aplikacija.domain.DopustenjePredmet;
import Zavrsni_rad.web_aplikacija.domain.Predmet;
import Zavrsni_rad.web_aplikacija.service.DopustenjePredmetService;
import Zavrsni_rad.web_aplikacija.service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/dopustenjePredmet")
public class DopustenjePredmetController {
    @Autowired
    private DopustenjePredmetService dopustenjePredmetService;
    @Autowired
    private KorisnikService korisnikService;

    @GetMapping("")
    public List<DopustenjePredmet> listDopustenjePredmet(){
        return dopustenjePredmetService.listAll();
    }

    @GetMapping("/{imeKorisnik}")
    public List<Predmet> predmetZaKoristenje(@PathVariable String imeKorisnik){
        int idKorisnik = korisnikService.imeKorisnikToIdKorisnik(imeKorisnik);
        return  dopustenjePredmetService.predmetZaKoristenje(idKorisnik);
    }
}
