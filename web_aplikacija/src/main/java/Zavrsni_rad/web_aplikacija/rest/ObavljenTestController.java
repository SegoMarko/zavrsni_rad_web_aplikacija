package Zavrsni_rad.web_aplikacija.rest;

import Zavrsni_rad.web_aplikacija.domain.KvizPovijest;
import Zavrsni_rad.web_aplikacija.domain.ObavljenTest;
import Zavrsni_rad.web_aplikacija.domain.PitanjePovijest;
import Zavrsni_rad.web_aplikacija.domain.TestInfo;
import Zavrsni_rad.web_aplikacija.service.ObavljenTestService;
import Zavrsni_rad.web_aplikacija.service.PitanjePovijestService;
import Zavrsni_rad.web_aplikacija.service.RezultatStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/obavljenTest")
public class ObavljenTestController {
    @Autowired
    private ObavljenTestService obavljenTestService;
    @Autowired
    private PitanjePovijestService pitanjePovijestService;
    @Autowired
    private RezultatStudentService rezultatStudentService;

    @GetMapping("/{idKorisnik}")
    public List<ObavljenTest> getKvizPovijestByIdKorisnik(@PathVariable int idKorisnik){
        return obavljenTestService.getKvizPovijestByIdKorisnik(idKorisnik);
    }

    @GetMapping("/pitanja/{idKvizPovijest}")
    public List<PitanjePovijest> getPitanjePovijestByIdObavljenTest(@PathVariable int idKvizPovijest){
        return pitanjePovijestService.getPitanjePovijestByIdKvizPovijest(idKvizPovijest);
    }

    @PostMapping("/info")
    public TestInfo testInfo(@RequestBody ObavljenTest obavljenTest){
        TestInfo ti = new TestInfo();

        ti.setBrStud(rezultatStudentService.getBrStud(obavljenTest.getIdObavljenTest()));
        List<PitanjePovijest> pitanja = pitanjePovijestService.getPitanjePovijestByIdKvizPovijest(obavljenTest.getIdKvizPovijest());
        int suma=0;
        for(PitanjePovijest pitanje : pitanja){
            suma+=pitanje.getBodToc();
        }
        ti.setMaxBod(suma);
        int tocniBodovi = rezultatStudentService.getTocniBodovi(obavljenTest.getIdObavljenTest());
        int netocniBodovi = rezultatStudentService.getNetocniBodovi(obavljenTest.getIdObavljenTest());
        //System.out.println("tocni bodovi:"+tocniBodovi+"\nnetocni bodovi:"+netocniBodovi);
        int neodgovoreniBodovi = rezultatStudentService.getNeodgovoreniBodovi(obavljenTest.getIdObavljenTest());
        if(ti.getBrStud()>0)
            ti.setProsjek((tocniBodovi+netocniBodovi+neodgovoreniBodovi)/(float)ti.getBrStud());
        else
            ti.setProsjek(0);
        Map<Integer, Float> pitanjaProsjek = new HashMap<>();
        if(ti.getBrStud()>0){
            for(PitanjePovijest pitanje : pitanja){
                pitanjaProsjek.put(pitanje.getIdPitanjePovijest(),
                        rezultatStudentService.getBrStudTocnoOdg(obavljenTest.getIdObavljenTest(), pitanje.getIdPitanjePovijest())*100/((float) ti.getBrStud()));
            }
        }else{
            for(PitanjePovijest pitanje : pitanja){
                pitanjaProsjek.put(pitanje.getIdPitanjePovijest(), (float)0);
            }
        }

        ti.setPitanjaProsjek(pitanjaProsjek);

        return ti;
    }
}
