package Zavrsni_rad.web_aplikacija.rest;

import Zavrsni_rad.web_aplikacija.domain.DopustenjePitanje;
import Zavrsni_rad.web_aplikacija.domain.Pitanje;
import Zavrsni_rad.web_aplikacija.domain.PonudeniOdgovor;
import Zavrsni_rad.web_aplikacija.service.DopustenjePitanjeService;
import Zavrsni_rad.web_aplikacija.service.PitanjeService;
import Zavrsni_rad.web_aplikacija.service.PonudeniOdgovorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/pitanje")
public class PitanjeController {
    @Autowired
    private PitanjeService pitanjeService;
    @Autowired
    private DopustenjePitanjeService dopustenjePitanjeService;
    @Autowired
    private PonudeniOdgovorService ponudeniOdgovorService;

    @GetMapping("/{idKviz}/{idKorisnik}")
    public List<Pitanje> getPitanja(@PathVariable int idKviz, @PathVariable int idKorisnik){
        return pitanjeService.getPitanja(idKviz, idKorisnik);
    }

    @GetMapping("/pitanje/{idPitanje}")
    public Pitanje getPitanje(@PathVariable int idPitanje){
        return pitanjeService.getPitanje(idPitanje);
    }

    @PostMapping("/{idKorisnik}/{idKviz}")
    public Pitanje addPitanje(@PathVariable int idKorisnik,@PathVariable int idKviz , @RequestBody Pitanje pitanje){
        pitanje.setIdVrstaPitanje('s');
        Pitanje p = pitanjeService.savePitanje(pitanje);

        dopustenjePitanjeService.saveDopustenje(new DopustenjePitanje(idKorisnik, p.getIdPitanje(),'o'));
        pitanjeService.saveKvizPitanje(p.getIdPitanje(), idKviz);

        return p;
    }

    @PutMapping("/{idKorisnik}")
    public Pitanje savePitanje(@RequestBody Pitanje pitanje){
        return this.pitanjeService.savePitanje(pitanje);
    }

    @PostMapping("")
    public PonudeniOdgovor addPonudeniOdgovor(@RequestBody PonudeniOdgovor ponudeniOdgovor){
        return this.ponudeniOdgovorService.addPonudeniOdgovor(ponudeniOdgovor);
    }

    @DeleteMapping("/{idKviz}/{idPitanje}")
    public void removePitanjeFromKviz(@PathVariable int idPitanje, @PathVariable int idKviz){
        this.pitanjeService.removePitanjeFromKviz(idPitanje, idKviz);
    }

    @DeleteMapping("/{idOdgovor}")
    public void deletePonudeniOdgovor(@PathVariable int idOdgovor){
        this.ponudeniOdgovorService.deletePonudeniOdgovor(idOdgovor);
    }
}
