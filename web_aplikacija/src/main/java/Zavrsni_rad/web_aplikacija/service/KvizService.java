package Zavrsni_rad.web_aplikacija.service;

import Zavrsni_rad.web_aplikacija.domain.Kviz;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface KvizService {
    List<Kviz> listAll();

    Kviz saveKviz(Kviz kviz);

    Kviz getKvizById(int idKviz);

    void deleteKviz(int idKviz);

    List<Kviz> getKvizByIdNastCjel(int idNastavnaCjelina);

    Kviz[] findByImeKviz(String imeKviz, int idKorisnik);

    List<Kviz> getKvizByIdNastCjelCustom(int idNastavnaCjelina, int idKorisnik);
}
