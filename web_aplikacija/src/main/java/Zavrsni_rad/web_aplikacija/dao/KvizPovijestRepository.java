package Zavrsni_rad.web_aplikacija.dao;

import Zavrsni_rad.web_aplikacija.domain.KvizPovijest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KvizPovijestRepository extends JpaRepository<KvizPovijest,Integer> {

}
