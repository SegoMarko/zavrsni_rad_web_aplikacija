package Zavrsni_rad.web_aplikacija.service.impl;

import Zavrsni_rad.web_aplikacija.dao.PitanjePovijestRepository;
import Zavrsni_rad.web_aplikacija.domain.PitanjePovijest;
import Zavrsni_rad.web_aplikacija.service.PitanjePovijestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PitanjePovijestJpa implements PitanjePovijestService {
    @Autowired
    private PitanjePovijestRepository pitanjePovijestRepository;

    @Override
    public List<PitanjePovijest> getPitanjePovijestByIdKvizPovijest(int idKvizPovijest) {
        return pitanjePovijestRepository.findByIdKvizPovijest(idKvizPovijest);
    }
}
