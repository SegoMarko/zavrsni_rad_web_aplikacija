package Zavrsni_rad.web_aplikacija.dao;

import Zavrsni_rad.web_aplikacija.domain.DopustenjeKviz;
import Zavrsni_rad.web_aplikacija.domain.DopustenjeKvizKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DopustenjeKvizRepository extends JpaRepository<DopustenjeKviz, DopustenjeKvizKey> {
}
