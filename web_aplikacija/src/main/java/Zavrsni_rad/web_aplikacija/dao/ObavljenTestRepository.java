package Zavrsni_rad.web_aplikacija.dao;

import Zavrsni_rad.web_aplikacija.domain.KvizPovijest;
import Zavrsni_rad.web_aplikacija.domain.ObavljenTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ObavljenTestRepository extends JpaRepository<ObavljenTest, Integer> {
    List<ObavljenTest> findByIdKorisnik(int idKorisnik);
}
