package Zavrsni_rad.web_aplikacija.dao;

import Zavrsni_rad.web_aplikacija.domain.NastavnaCjelina;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NastavnaCjelinaRepository extends JpaRepository<NastavnaCjelina, Integer> {
    List<NastavnaCjelina> findByIdPredmet(int idPredmet);

    List<NastavnaCjelina> findByNazNastCjelContaining(String nazNastCjel);

    @Query("SELECT nc FROM NastavnaCjelina nc JOIN DopustenjeCjelina dc" +
            " ON nc.idNastavnaCjelina=dc.idNastavnaCjelina" +
            " WHERE dc.idKorisnik=(:idKorisnik)" +
            " AND nc.nazNastCjel LIKE  CONCAT('%',:nazNastCjel,'%')")
    List<NastavnaCjelina> findByNazNastCjelCustom(@Param("nazNastCjel") String nazNastCjel, @Param("idKorisnik") int idKorisnik);

    @Query("SELECT nc FROM NastavnaCjelina nc JOIN DopustenjeCjelina dc" +
            " ON nc.idNastavnaCjelina=dc.idNastavnaCjelina" +
            " WHERE nc.idPredmet=(:idPredmet)" +
            " AND dc.idKorisnik=(:idKorisnik)")
    List<NastavnaCjelina> findByIdPredmetCustom(@Param("idPredmet") int idPredmet, @Param("idKorisnik") int idKorisnik);
}
