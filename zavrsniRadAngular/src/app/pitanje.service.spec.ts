import { TestBed } from '@angular/core/testing';

import { PitanjeService } from './pitanje.service';

describe('PitanjeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PitanjeService = TestBed.get(PitanjeService);
    expect(service).toBeTruthy();
  });
});
