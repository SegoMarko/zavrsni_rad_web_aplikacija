import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PomPorukeComponent } from './pom-poruke.component';

describe('PomPorukeComponent', () => {
  let component: PomPorukeComponent;
  let fixture: ComponentFixture<PomPorukeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PomPorukeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PomPorukeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
