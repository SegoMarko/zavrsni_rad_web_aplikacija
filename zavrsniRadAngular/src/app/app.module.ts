import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PregledComponent } from './pregled/pregled.component';
import { FormsModule } from '@angular/forms';
import { KvizDetailComponent } from './kviz-detail/kviz-detail.component';
import { PredmetDetailComponent } from './predmet-detail/predmet-detail.component';
import { PomPorukeComponent } from './pom-poruke/pom-poruke.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PredmetSearchComponent } from './predmet-search/predmet-search.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NastavnaCjelinaDetailComponent } from './nastavna-cjelina-detail/nastavna-cjelina-detail.component';
import { NastavnaCjelinaSearchComponent } from './nastavna-cjelina-search/nastavna-cjelina-search.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgxPaginationModule } from 'ngx-pagination';
import { RezultatiComponent } from './rezultati/rezultati.component';

@NgModule({
  declarations: [
    AppComponent,
    PregledComponent,
    KvizDetailComponent,
    PredmetDetailComponent,
    PomPorukeComponent,
    DashboardComponent,
    PredmetSearchComponent,
    NastavnaCjelinaDetailComponent,
    NastavnaCjelinaSearchComponent,
    RezultatiComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    FormsModule,
    NgxPaginationModule,
    AngularFontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
