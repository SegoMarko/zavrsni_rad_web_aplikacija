package Zavrsni_rad.web_aplikacija.service.impl;

import Zavrsni_rad.web_aplikacija.dao.KorisnikRepository;
import Zavrsni_rad.web_aplikacija.domain.Korisnik;
import Zavrsni_rad.web_aplikacija.service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KorisnikServiceJpa implements KorisnikService {
    @Autowired
    private KorisnikRepository korisnikRepository;

    @Override
    public List<Korisnik> listAll(){
        return korisnikRepository.findAll();
    }

    @Override
    public Integer imeKorisnikToIdKorisnik(String imeKorisnik) {
        return korisnikRepository.findByImeKorisnik(imeKorisnik).getIdKorisnik();
    }
}
