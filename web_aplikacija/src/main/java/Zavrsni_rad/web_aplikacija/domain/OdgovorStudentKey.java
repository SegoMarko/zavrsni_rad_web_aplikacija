package Zavrsni_rad.web_aplikacija.domain;

import java.io.Serializable;

public class OdgovorStudentKey implements Serializable {
    private int idStudent;
    private int idTest;
    private int idOdgovor;
}
