package Zavrsni_rad.web_aplikacija.dao;

import Zavrsni_rad.web_aplikacija.domain.DopustenjePitanje;
import Zavrsni_rad.web_aplikacija.domain.DopustenjePitanjeKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DopustenjePitanjeRepository extends JpaRepository<DopustenjePitanje, DopustenjePitanjeKey> {
}
