package Zavrsni_rad.web_aplikacija.dao;

import Zavrsni_rad.web_aplikacija.domain.KvizPitanje;
import Zavrsni_rad.web_aplikacija.domain.KvizPitanjeKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KvizPitanjeRepository extends JpaRepository<KvizPitanje, KvizPitanjeKey> {
}
