package Zavrsni_rad.web_aplikacija.domain;

import java.util.Comparator;

public class KvizComparator implements Comparator<Kviz> {
    @Override
    public int compare(Kviz o1, Kviz o2) {
        if (o1.getRedniBrojKviz()>o2.getRedniBrojKviz())
            return 1;
        else if(o1.getRedniBrojKviz()==o2.getRedniBrojKviz())
            return 0;
        else
            return -1;
    }
}
